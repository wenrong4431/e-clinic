# E-Clinic

E-Clinic is a to improve clinic management efficiency and let doctor have a better understand to his patient. This system is written using spring boot framework.

**Feature:**
* [ ] For doctor to record patient's health condition on every appointment & analysis health condition such as blood sugar & high blood pressure
* [ ] For remind patient on appointment time & doing health test
* [ ] For doctor to share current popular illness such as food poinising and influenza to avoid misdiagnosis
* [ ] For clinic to record medicine price & location to avoid find out requried medicine for patient
* [ ] For patient to share his/her blood sugar / high blood pressure / current condition / others to doctor for better tracking health condition
* [ ] Secure & separate patient's health condition between each clinic
* [ ] Contact clinic for emergency condition
* [ ] Patient can share his / her blood test report / others & disease history to doctor
